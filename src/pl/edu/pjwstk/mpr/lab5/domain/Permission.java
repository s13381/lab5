package pl.edu.pjwstk.mpr.lab5.domain;


public class Permission {
    private String name;

    public String getName() {
        return name;
    }

    public Permission setName(String name) {
        this.name = name;
        return this;
    }
}
